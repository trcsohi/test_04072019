﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Test.Game;

namespace Test.EditorUtils {

    /// <summary>
    /// Custom inspector for crosshair
    /// </summary>
    [CustomEditor(typeof(Crosshair))]
    public class CrosshairInspector : Editor {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var crosshairComponent = (Crosshair)target;
            GUILayout.Label($"Active : {crosshairComponent.IsActive}");
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Test.Utils;

namespace Test.Game
{

    [System.Serializable]
    public class PanzerInputSetting : Setting<PanzerActions>
    {
    }

    [CreateAssetMenu(fileName = "PanzerInput", menuName = "Input/PanzerInput")]
    public class PanzerInput : ScriptableObject, IInputSettings<PanzerActions> 
    {
        [SerializeField]
        private List<PanzerInputSetting> settings = new List<PanzerInputSetting>();

        private Dictionary<PanzerActions, string> input = default;

        public IReadOnlyDictionary<PanzerActions, string> Input => input;
        private void OnEnable()
        {
            SetDictionary();
        }

        private void SetDictionary()
        {
            input = new Dictionary<PanzerActions, string>();

            foreach (var setting in settings)
            {
                input.Add(setting.Action, setting.Input);
            }

        }
    }
}

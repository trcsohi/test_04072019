﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Test.Helpers;

namespace Test.Game {

    /// <summary>
    /// describes an object which rotation depends on target position, fires a bullet
    /// </summary>

    public class Gun : MonoBehaviour {
        private PseudoPanzer parent = default;
        public Action<float> HorizontalOverflow = default;

        Vector3 Target => CrosshairController.Instance.CrosshairPosition;

        [SerializeField]
        AmmoSpawner spawner = default;

        public GameObject Spawner => spawner.gameObject;

        private float VerticalAngle {

            get {
                var angle = TrajectoryController.Instance.Angle;
                return angle != null ? Mathf.Clamp((float)angle, 0, GameController.Instance.MaxGunVerticalAngle) : 0;
            }
            
        }
        private void Start()
        {
            parent = GetComponentInParent<PseudoPanzer>();
        }

        void Update()
        {
            FollowTarget();
        }


        void FollowTarget()
        {
            transform.eulerAngles = new Vector3(360 - VerticalAngle, 0f, 0f);
            var horizontalAngle = GetHorizontalAngle();
            float maxAngleH = GameController.Instance.MaxGunHorizontalAngle;
            var localHorizontalAngle = horizontalAngle - transform.parent.eulerAngles.y;
            var hanormlized = GetBetween180(localHorizontalAngle);

            if (Mathf.Abs(hanormlized) > maxAngleH)
            {
                float newHorizontal = Mathf.Clamp(hanormlized, (-maxAngleH + transform.rotation.y),(maxAngleH + transform.rotation.y));

                float range = GetBetween180(hanormlized - newHorizontal);

                if (Mathf.Abs(range) > Mathf.Epsilon)
                {
                    HorizontalOverflow?.Invoke(range);
                    horizontalAngle = !parent.IsMoving ? GetHorizontalAngle() : newHorizontal + transform.parent.eulerAngles.y;
                }
            }

            transform.eulerAngles = new Vector3(transform.eulerAngles.x, horizontalAngle, 0f);
        }

        float GetBetween180(float angle)
        {
            var result = Mathf.Abs(angle) >= 180 ?
                (angle < 0 ? angle + 360 : angle - 360)
                : angle;
            if (Mathf.Abs(result) >= 180)
            {
                return GetBetween180(result);
            }
            else
            {
                return result;
            }
        }

        float GetHorizontalAngle()
        {
            var direction = Target - transform.position;
            var xzProj = Vector3.ProjectOnPlane(direction, Vector3.up);
            return Quaternion.LookRotation(xzProj).eulerAngles.y;
        }

        public void Fire()
        {
            var velocity = transform.TransformDirection(TrajectoryController.Instance.Velocity);
            spawner.Spawn(velocity);
        }

    }
}

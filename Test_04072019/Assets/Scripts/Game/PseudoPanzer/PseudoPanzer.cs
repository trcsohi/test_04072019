﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Test.Game
{
    public enum PanzerActions
    {
        MOVE,
        TURN,
        FIRE,
    }

    /// <summary>
    /// Moving platform controller, change its transform and checks inout
    /// </summary>

    [RequireComponent(typeof(Rigidbody))]
    public sealed class PseudoPanzer : MonoBehaviour
    {
        [SerializeField]
        PanzerInput inputSettings = default;
        [Space]
        [SerializeField]
        float moveSpeed = 5f;
        [SerializeField]
        float turnSpeed = 2f;

        private Rigidbody rigidbodyComponent = default;
        private float moveValue = default;
        private float turnValue = default;

        [SerializeField]
        private Gun gun = default;

        public Gun Gun => gun;

        public bool IsMoving { get; private set; }

        private void Awake()
        {
            rigidbodyComponent = GetComponent<Rigidbody>();
            gun.HorizontalOverflow += OnHorizontalOverflow;
        }

        private void OnHorizontalOverflow(float value) {
            Rotate(value);
        }

        private void Start()
        {
            SetState();
        }

        private void SetState()
        {
            rigidbodyComponent.isKinematic = true;
            rigidbodyComponent.useGravity = false;
        }

        private void Update()
        {
            UpdateMovingValues();
        }

        private void OnDestroy() {
            gun.HorizontalOverflow -= OnHorizontalOverflow;
        }

        private void FixedUpdate()
        {
            Turn();
            Move();
            CheckFire();
        }

        private void CheckFire()
        {
            if (Input.GetButtonDown(inputSettings.Input[PanzerActions.FIRE]) && CrosshairController.Instance.CanShoot)
            {
                gun.Fire();
                Debug.Log("spawn");
            }
        }

        private void UpdateMovingValues()
        {
            moveValue = Input.GetAxis(inputSettings.Input[PanzerActions.MOVE]);
            turnValue = Input.GetAxis(inputSettings.Input[PanzerActions.TURN]);
            IsMoving = moveValue > 0 || turnValue > 0;
        }

        private void Move()
        {
            var moving = moveValue * transform.forward * moveSpeed * Time.fixedDeltaTime;
            rigidbodyComponent.MovePosition(moving + rigidbodyComponent.position);
        }

        private void Turn()
        {
            var rotation = Quaternion.Euler(0f, turnValue * turnSpeed * Time.fixedDeltaTime, 0f);

            rigidbodyComponent.MoveRotation(rotation * rigidbodyComponent.rotation);
        }

        private void Rotate(float degree) {

            if (!IsMoving) transform.eulerAngles = new Vector3(transform.eulerAngles.x, (transform.eulerAngles.y + degree), transform.eulerAngles.z);
        }



#if UNITY_EDITOR
        private void OnEnable()
        {
            Assert.IsNotNull(inputSettings, "No settings attached!");
        }
#endif
    }
}

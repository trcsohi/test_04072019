﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Test.Utils;

namespace Test.Game {

    /// <summary>
    /// Checks if we can shoot and shows the crosshair
    /// </summary>

    public class CrosshairController : MonoSingleton<CrosshairController> {

        private readonly string allowedSurface = "Ground";

        [SerializeField]
        Crosshair crosshair = default;

        public GameObject Crosshair => crosshair.gameObject;

        private float crosshairYPos = default;

        public bool CanShoot => crosshair.IsActive;

        public Vector3 CrosshairPosition => crosshair.transform.position;

        private void Start()
        {
            crosshairYPos = crosshair.transform.position.y;
        }

        private void Update()
        {
            UpdateCrosshairPosition();
        }

        private void UpdateCrosshairPosition()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                bool canReach = TrajectoryController.Instance.Angle <= GameController.Instance.MaxGunVerticalAngle;

                crosshair.IsActive = hit.collider.gameObject.tag == allowedSurface && canReach;
                crosshair.transform.position = hit.point + Vector3.up * crosshairYPos;
  
            }
        }
    }
}

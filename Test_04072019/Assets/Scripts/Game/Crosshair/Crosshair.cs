﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test.Game {
    /// <summary>
    /// Crosshair object script : allows to change crosshair's view
    /// </summary>

    [RequireComponent(typeof(SpriteRenderer))]
    public class Crosshair : MonoBehaviour {

        [SerializeField]
        Sprite inactive = default;
        [SerializeField]
        Sprite active = default;

        private SpriteRenderer rendererComponent = default;

        private bool isActive = default;

        public bool IsActive {
            get => isActive;
            set {
                isActive = value;
                rendererComponent.sprite = isActive ? active : inactive;
            }
        }

        private void Awake()
        {
            rendererComponent = GetComponent<SpriteRenderer>();
        }

    }
}

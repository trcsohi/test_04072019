﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test.Helpers {
    /// <summary>
    /// 
    /// </summary>

    public static class TrajectoryHelper {

        public static float? CalculateAngle(Vector3 from, Vector3 target, float speed = 20f)
        {
            float? highAngle = null;
            float? lowAngle = null;
            Vector3 distance = target - from;
            float y = distance.y;
            distance.y = 0f;
            float x = distance.magnitude;
            float g = -Physics.gravity.y;
            float vSqr = speed * speed;
            float underTheRoot = (vSqr * vSqr) - g * (g * x * x + 2 * y * vSqr);

            if (underTheRoot >= 0f)
            {
                float rightSide = Mathf.Sqrt(underTheRoot);

                float top1 = vSqr + rightSide;
                float top2 = vSqr - rightSide;

                float bottom = g * x;

                highAngle = Mathf.Atan2(top1, bottom) * Mathf.Rad2Deg;
                lowAngle = Mathf.Atan2(top2, bottom) * Mathf.Rad2Deg;
            }
            return highAngle != null && lowAngle != null ? (highAngle - lowAngle) / 2 : null;
        }

        public static Vector3 GetVelocity(Vector3 target, Vector3 from, float angle)
        {
            var distance = target - from;
            var x = new Vector3(distance.x, 0f, distance.z).magnitude;
            var y = distance.y;

            float g = Physics.gravity.y;
            float tanAlpha = Mathf.Tan(angle * Mathf.Deg2Rad);

            float v = Mathf.Abs(Mathf.Sqrt((g * x * x / (2f * (y - x * tanAlpha) * Mathf.Pow(Mathf.Cos(angle * Mathf.Deg2Rad), 2)))));

            return Vector3.forward * v;
        }

    }
}

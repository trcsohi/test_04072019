﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Test.Utils;
using Test.Helpers;

namespace Test.Game {

    public class TrajectoryController : MonoSingleton<TrajectoryController> {

        private GameObject target = default;

        private GameObject spawner = default;

        public float? Angle {
            get {
                return TrajectoryHelper.CalculateAngle(spawner.transform.position, target.transform.position);
            }
        }

        public Vector3 Velocity {
            get {
                return TrajectoryHelper.GetVelocity(target.transform.position, spawner.transform.position, Angle != null ? (float)Angle : 0);
            }
        }

        public void Init(GameObject target, GameObject spawner) { 

            this.target = target;
            this.spawner = spawner;
        }


    }
}

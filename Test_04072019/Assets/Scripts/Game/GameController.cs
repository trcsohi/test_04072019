﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Test.Utils;

namespace Test.Game {


    public class GameController : MonoSingleton<GameController>  {
        [SerializeField]
        [Range(0, 90)]
        private float maxGunVerticalAngle = 80f;

        [SerializeField]
        [Range(0, 90)]
        private float maxGunHorizontalAngle = 20f;

        [SerializeField]
        bool hasTrail = true;

        public float MaxGunVerticalAngle => maxGunVerticalAngle;

        public float MaxGunHorizontalAngle => maxGunHorizontalAngle;

        public bool HasTrail => hasTrail;

        private List<AmmoType> availibleAmmo = new List<AmmoType>()
        {
            AmmoType.CANNONBALL,
        };

        private void Start()
        {
            var panzer = FindObjectOfType<PseudoPanzer>();
            if (panzer != null)
            {
                TrajectoryController.Instance.Init(CrosshairController.Instance.Crosshair, panzer.Gun.Spawner);
                AmmoPool.Instance.Init(availibleAmmo);
            }
            else
            {
                Debug.LogError("Panzer object can not be found");
            }
        }
    }
}

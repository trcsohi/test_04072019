﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Describes main following camera (only follows target without rotation)
/// </summary>

[RequireComponent(typeof(Camera))]
public class FollowingCamera : MonoBehaviour
{
    [SerializeField]
    GameObject target = default;
    [Space]
    float smoothValue = 2f;

    Vector3 offset = default;

    private void Start()
    {
        offset = transform.position - target.transform.position;
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, target.transform.position + offset, smoothValue * Time.fixedDeltaTime);
    }
}

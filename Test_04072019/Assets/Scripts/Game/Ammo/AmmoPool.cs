﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Test.Utils;

namespace Test.Game {

    /// <summary>
    /// Pool for all ammunition
    /// </summary>
    public class AmmoPool : ObjectsPool<AmmoType, Ammo> {

    }

}


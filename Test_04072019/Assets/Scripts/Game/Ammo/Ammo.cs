﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Test.Utils;
using System;

namespace Test.Game {

    /// <summary>
    /// base class for all ammunition (we have only a cannonball)
    /// </summary>

    public class Ammo : FactoryItem<AmmoType> {
        public Action<Ammo> GroundTouched = default;

        [SerializeField]
        private AmmoType type = default;

        public override AmmoType Type => type;

        Rigidbody rigidbodyComponent = default;

        TrailRenderer trailRendererComponent = default;

        private void Awake() {
            rigidbodyComponent = GetComponent<Rigidbody>();
            trailRendererComponent = GetComponent<TrailRenderer>();
        }


       public IEnumerator DoPath(Vector3 target) {

            while (transform.position.y > AmmoPool.Instance.transform.position.y) {

                if (Physics.Raycast(transform.position, target, out RaycastHit hit)) {
                    OnTrigger(hit.collider);
                    yield break;
                }
                yield return null;
            }
            GroundTouched?.Invoke(this);
            yield return null;
        }

        void OnTrigger(Collider other) {
            if (!other.gameObject.CompareTag("Player")) {
                GroundTouched?.Invoke(this);
            }
        }

        public override void Reset() {
            base.Reset();
            rigidbodyComponent.velocity = Vector3.zero;
            rigidbodyComponent.isKinematic = true;
            rigidbodyComponent.useGravity = false;
            trailRendererComponent.enabled = false;
            trailRendererComponent.Clear();
        }
        public override void OnStart() {
            base.OnStart();
            rigidbodyComponent.isKinematic = false;
            rigidbodyComponent.useGravity = true;
            trailRendererComponent.enabled = GameController.Instance.HasTrail;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test.Game {

    public class AmmoSpawner : MonoBehaviour {

        private List<Ammo> ammos = new List<Ammo>();


        public void Spawn(Vector3 velocity) {
            var ammo = AmmoPool.Instance.GetItem(AmmoType.CANNONBALL, this.transform);
            ammo.GroundTouched += OnAmmoTouchedGround;

            ammo.GetComponent<Rigidbody>().velocity = velocity;
            ammo.transform.parent = null;
            ammos.Add(ammo);
            StartCoroutine(ammo.DoPath(CrosshairController.Instance.CrosshairPosition));
            
        }
        void OnAmmoTouchedGround(Ammo ammo) {
            ammo.Reset();
            AmmoPool.Instance.Return(ammo);
            ammo.GroundTouched -= OnAmmoTouchedGround;
            ammos.Remove(ammo);
        }
    }
}

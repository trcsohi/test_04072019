﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test.Utils
{
    [System.Serializable]
    public class Setting<T> where T : Enum
    {
        [SerializeField]
        private T action = default;

        [SerializeField]
        private string input = default;

        public T Action => action;

        public string Input => input;
    }

    public interface IInputSettings<T> where T : Enum
    {
        IReadOnlyDictionary<T, string> Input { get; }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test.Utils {
    public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour {

        private static T instance;

        public static T Instance {
            get {
                if (instance != null)
                {
                    return instance;
                }

                instance = FindObjectOfType<T>();
                if (instance != null)
                {
                    return instance;
                }

                Type type = typeof(T);
                var singleton = new GameObject(string.Format("[{0}]", type.Name));
                instance = singleton.AddComponent<T>();

                return instance;
            }
        }

        protected virtual void Awake()
        {
            if (instance == null)
            {
                instance = this as T;
            }
        }
    }
}

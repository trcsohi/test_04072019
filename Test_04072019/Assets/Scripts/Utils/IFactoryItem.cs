﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test.Utils
{
    public interface IFactoryItem<ItemType> where ItemType : System.Enum
    {
        ItemType Type { get; }
        void Reset();
        void OnStart();
    }


    public class FactoryItem<ItemType> : MonoBehaviour, IFactoryItem<ItemType> where ItemType : System.Enum
    {
        public virtual ItemType Type { get; }

        public virtual void OnStart() {
            
        }

        public virtual void Reset() {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
    }
}

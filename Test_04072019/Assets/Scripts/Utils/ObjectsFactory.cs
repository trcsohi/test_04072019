﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test.Utils
{
    public interface IFactory<Key, Value> where Key : System.Enum
        where Value : MonoBehaviour, IFactoryItem<Key>
    {
        Value GetItem(Key type);
        Value GetItem(Key type, Transform parent);
    }

    public class ObjectsFactory<Key, Value> : MonoBehaviour, IFactory<Key, Value> where Key : System.Enum
        where Value : MonoBehaviour, IFactoryItem<Key>
    {
        [SerializeField]
        List<Value> values = default;

        Dictionary<Key, Value> valuesObjects = default;

        private void Awake()
        {
            valuesObjects = new Dictionary<Key, Value>();
            foreach (var prefab in values)
            {
                valuesObjects.Add(prefab.Type, prefab);
            }
        }

        public Value GetItem(Key type, Transform parent)
        {
            return Instantiate(valuesObjects[type], parent);
        }

        public Value GetItem(Key type)
        {
            return Instantiate(valuesObjects[type]);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test.Utils
{
    public class ObjectsPool<Key, Value> : MonoSingleton<ObjectsPool<Key, Value>> where Key : System.Enum
        where Value : MonoBehaviour, IFactoryItem<Key>
    {
        private Dictionary<Key, Stack<Value>> objects = default;


        private ObjectsFactory<Key, Value> factory = default;

        protected override void Awake()
        {
            base.Awake();
            factory = GetComponent<ObjectsFactory<Key,Value>>();
        }

        public void Init(IReadOnlyList<Key> availibleKeys)
        {
            objects = new Dictionary<Key, Stack<Value>>();

            foreach (var key in availibleKeys)
            {
                objects.Add(key, new Stack<Value>());
            }
        }

        public Value GetItem(Key type, Transform parent)
        {
            var typeList = objects[type];
            if (typeList.Count == 0)
            {
                typeList.Push(factory.GetItem(type, parent));
            }
            var item = typeList.Pop();
            item.transform.parent = parent;
            item.transform.localPosition = Vector3.zero;
            item.OnStart();

            return item;
        }

        public void Return(Value item)
        {
            var typeList = objects[item.Type];

            item.transform.parent = transform;
            item.Reset();

            typeList.Push(item);
        }

    }
}